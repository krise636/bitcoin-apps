/**
 * Build Funding Transaction
 *
 * NOTE: We may not need this function since the web browser wallet will be
 *       receiving funds in a single txn.
 */
const buildFundingTx = function (_config) {
    /* Initialize transaction builder. */
    let transactionBuilder

    /* Handle networking. */
    transactionBuilder = new this.BITBOX.TransactionBuilder('bitcoincash')

    /* Initialize satoshis. */
    let satoshis = 0

    /* Handle UTXOs. */
    _config.input_utxos.forEach(token_utxo => {
        transactionBuilder.addInput(token_utxo.txid, token_utxo.vout)
        satoshis += token_utxo.satoshis
    })

    /* Calculate funding miner fee. */
    const fundingMinerFee = this.BITBOX.BitcoinCash
        .getByteCount({ P2PKH: _config.input_utxos.length }, { P2PKH: 1 })

    /* Calculate output amount. */
    const outputAmount = satoshis - fundingMinerFee

    //assert config.fundingAmountSatoshis === outputAmount //TODO: Use JS syntax and throw on error

    // Output exact funding amount
    transactionBuilder.addOutput(_config.outputAddress, outputAmount)

    /* Initilize inputs index. */
    let i = 0

    for (const txo of _config.input_utxos) {
        /* Set payment keypair. */
        const paymentKeyPair = this.BITBOX.ECPair.fromWIF(txo.wif)

        /* Sign transaction. */
        transactionBuilder.sign(
            i,
            paymentKeyPair,
            undefined,
            transactionBuilder.hashTypes.SIGHASH_ALL,
            txo.satoshis
        )

        /* Increment i. */
        i++
    }

    /* Return "built" transaction. */
    return transactionBuilder.build()
}

/* Export module. */
module.exports = buildFundingTx
