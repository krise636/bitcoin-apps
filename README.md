![Vote for Unstoppable Web](https://i.imgur.com/vPlfFtj.png)

_Like our project? Send your CCK tokens to: [simpleledger:qqxtz0fw3gs5ndmwjm2we92k20zu3z99uuuxr25a2s](https://explorer.bitcoin.com/bch/address/simpleledger:qqxtz0fw3gs5ndmwjm2we92k20zu3z99uuuxr25a2s)_

# Unstoppable Web (Bitcoin Apps)

https://bitcoinapps.info

## FINAL PRESENTATION

Watch our final presentation here:

- https://vimeo.com/501955481
- https://www.youtube.com/watch?v=mrnvJTf-HlU
- [LBRY](https://lbry.tv/@BitcoinCashSite:6/The-Unstoppable-Web-(Bitcoin-Apps)-Final-Presentation-for-the-CoinParty-Hackathon-18-Jan-2021:6)
- [Facebook](https://www.facebook.com/BitcoinCashSite/posts/208964000890917)

## Inspiration

Over 20 years ago, a revolutionary peer-to-peer (P2P) application called Napster transformed the music industry forever. Advanced P2P protocols like BitTorrent extended these applications to allow for censorship-resistant applications like Popcorn Time (https://getpopcorntime.is/) to flourish. __Bitcoin Apps (https://bitcoinapps.info/) is the next stop on this P2P journey by introducing a new HTML format that can be served directly from the Bitcoin BCH blockchain.__

__Welcome to the UNSTOPPABLE Web__ 🥳

## What it does

Using the Bitcoin Files Protocol (BFP) and a new markup language called Bitcoin Markup Language (BML), application developers can now HOST & DELIVER their HTML, JavaScript and CSS directly from the Bitcoin BCH blockchain. BFP offers a censorship-resistant experience similar to BitTorrent, but unlike BT (and even TOR for that matter), it doesn't require files to be hosted ANYWHERE.

This all makes Bitcoin Apps impossible to shut down. So long as the Bitcoin BCH blockchain exists, these applications will be served to ANY compatible client, FOREVER.

### Standard HTML

```
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>My First BML Page</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" integrity="sha512-+4zCK9k+qNFUR5X+cKL9EIR+ZOhtIloNl9GIKS57V1MyNsYpYcUrUeQc9vNfzsWfV28IaLL3i96P9sdNyeRssA==" crossorigin="anonymous" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.3/css/bootstrap.min.css" integrity="sha512-oc9+XSs1H243/FRN9Rw62Fn8EtxjEYWHXRvjS43YtueEewbS6ObfXcJNyohjHqVKFPoXXUxwc+q1K7Dee6vv9g==" crossorigin="anonymous" />
</head>

<body>
<h1>Hello Bitcoin!</h1>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.3/js/bootstrap.min.js" integrity="sha512-8qmis31OQi6hIRgvkht0s6mCOittjMa9GMqtK9hes5iEQBQE/Ca6yGE5FsW36vyipGoWQswBj/QBm2JR086Rkw==" crossorigin="anonymous"></script>
<script src="app.js"></script>
</body>
</html>
```

<strong class="text-danger">[ 1,118 bytes in total ]</strong>

---

### Bitcoin Markup Language (BML)

```
# Bitcoin Markup Language (BML) Sample
title: My First BML Webpage
css: [font-awesome@5.15.1, twitter-bootstrap@4.5.3]
body: <h1>Hello Bitcoin!</h1>
libs: [jquery@3.5.1, twitter-bootstrap@4.5.3, QmcQy4qCGt7rxgRiW7A55TP1bEbC9vFHERPVMPpGhMpMWe]
```

<strong class="text-danger">[ 199 bytes in total ]</strong>

<small>
    <strong>NOTE:</strong> A single OP_RETURN can store up to 220 bytes.
    <br /><strong>50</strong> (max unconfirmed) transactions X <strong>220</strong> bytes = <strong>~10,000</strong> bytes
    <br />Max cost is <strong>0.00019486</strong> sats @ $350 = <strong>~$0.07</strong> USD
</small>

## How we built it

In order to support the maximum file size of 10k bytes, imposed due to the 50 unconfirmed transaction limit, our team will be creating a new markup language called __Bitcoin Markup Language (BML).__ This new language will be modeled after YAML (https://yaml.org/).

## Challenges we ran into

The 10k bytes file limit requires rethinking the structure of an HTML document:

1. An entirely new CDN library will be created to support the new markup language.

## Accomplishments that we're proud of

A simple tool for converting an existing Single Page Application (SPA) into a Bitcoin App is already underway and should be available for use by the end of this event.

## What we learned

Storing an optimized HTML in BML format is surprisingly inexpensive. An entire web application can be published for under US$1.00. Updates are far cheaper, as only the "modified" files would need to be re-uploaded.

## What's next for Bitcoin Apps

Nito Cloud will serve as the first web portal supporting the BML.

Dark Edge (https://darkedge.app/) will serve as the first mobile app supporting the BML. A fork of Edge (https://edge.app), Dark Edge will be an open-source solution for users to access this content from their mobile device.

## Frequently Asked Questions

**1. How does Unstoppable Web differ from IPFS?**

IPFS is great tech that we love and are incorporating as a complement into Unstoppable Web. For IPFS to work, it requires a web server behind it to store, or "pin", files for distribution. The Tor protocol has this dependency as well, which is why Tor hidden services can get taken down once they lose their hosting provider.

This is a lot like what happened to Parler recently when they lost their AWS hosting.

Unstoppable Web aims to remove the need for a centralized hosting provider. This is the same goal as the Filecoin project, which uses its own FIL token to pay for storage — while Unstoppable Web uses BCH. 😉👍

**2. How would a social network on Uncensorable Web differ from Noise.cash/Memo/Member?**

We're building a new protocol/platform that could potentially host all three projects, but in a serverless environment. This means there would be no web server that can be shut down. This would mean no Parlering of the projects.

Noise.cash is a great project gaining traction that has great UI, but does not have any on-chain components beyond the enormous amount of tipping happening there. Memo/Member hosts data on-chain and you have to pay to post. All of these projects have enormous potential, especially in a future where we see fewer large-scale centralized social networks controlled by Big Tech, and more of a long tail in this industry.

**3. Is Uncensorable Web like FileCoin?**

Indeed, it is, and we are building it on Bitcoin Cash where users will pay their transaction fees in Bitcoin Cash instead of another coin.

**4. Could Uncensorable Web have kept Parler from going offline when it was shut down?**

Yes, it could! And we would like to reach out to Parler and onboard them once the tech is ready.

**5. Is Uncensorable Web looking to host large files on the BCH blockchain?**

No. Data that is at a high risk of being censored would be more suitable for on-chain storage while low-risk data can use more convenient alternatives.

The Bitcoin Cash blockchain is AMAZING for hosting & delivering information that CANNOT be censored. So that's what we're focused on.

Uncensorable Web makes use of IPFS, particularly for large data files that are at a lower risk of censorship, e.g., software libraries such as jQuery, which is used by millions of web apps.

A dissident blogger's index.html, on the other hand, is better off on-chain. And that index.html is mostly just metadata. The purpose of the Bitcoin Markup Language (BML) is to pack more information into the 10 KB file limit and extend the number of applications that can be supported by the protocol.

It is actually impossible to put large files on the BCH blockchain as there is a 10 KB file-size limit due to the limit of 50 chained transactions.

**6. Is Uncensorable Web related to Unstoppable Domains or .zil?**

Not at this time but we are very open to integrations in the future!

**7. Which browsers will support it? Will there be a public portal?**

All modern browsers (including mobile) should work just fine, and we will test them to be sure.

Shomari is publishing a "public" web portal at [nito.cloud](https://nito.cloud) to get things off to a convenient start, but anyone can publish a node using [gitlab.com/bchplease/bitcoin-apps](https://gitlab.com/bchplease/bitcoin-apps).

[darkedge.app](https://darkedge.app) is the official "mobile" client, which is engineered to be essentially 100% resistant to censorship: [gitlab.com/bchplease/darkedge-app](https://gitlab.com/bchplease/darkedge-app).

We welcome further questions! Tweet [@ShomariPrince](http://twitter.com/shomariprince) or [@GeorgeDonnelly](https://twitter.com/georgedonnelly) or email [s.prince@bchplease.org](mailto:s.prince@bchplease.org) or [george@panmoni.com](mailto:george@panmoni.com).

## CoinParty Interview

Watch our CoinParty interview!

https://www.youtube.com/watch?v=pF1NlFH8ZMg

## CoinParty Voting

BCH  → [bitcoincash:qqvl7fwcthhhntsew056t8007pw55k258vmlm053fy](https://explorer.bitcoin.com/bch/address/bitcoincash:qqvl7fwcthhhntsew056t8007pw55k258vmlm053fy)

SLP  → [simpleledger:qqxtz0fw3gs5ndmwjm2we92k20zu3z99uuuxr25a2s](https://explorer.bitcoin.com/bch/address/simpleledger:qqxtz0fw3gs5ndmwjm2we92k20zu3z99uuuxr25a2s)

![SLP Address](https://i.imgur.com/MZuByqA.png)

(don't trust, verify @ https://bchplease.org)
